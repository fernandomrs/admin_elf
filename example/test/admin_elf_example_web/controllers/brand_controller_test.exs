defmodule AdminElfExampleWeb.BrandControllerTest do
  use AdminElfExampleWeb.ConnCase

  alias AdminElfExample.Cars
  alias AdminElfExample.Cars.Brand

  @create_attrs %{
    name: "some name"
  }
  @update_attrs %{
    name: "some updated name"
  }
  @invalid_attrs %{name: nil}

  def fixture(:brand) do
    {:ok, brand} = Cars.create_brand(@create_attrs)
    brand
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all brands", %{conn: conn} do
      conn = get(conn, Routes.brand_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  defp create_brand(_) do
    brand = fixture(:brand)
    %{brand: brand}
  end
end
