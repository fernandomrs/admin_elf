defmodule AdminElfExampleWeb.CarControllerTest do
  use AdminElfExampleWeb.ConnCase

  alias AdminElfExample.Cars
  alias AdminElfExample.Cars.Car

  @create_attrs %{
    name: "some name",
    year: 42
  }
  @update_attrs %{
    name: "some updated name",
    year: 43
  }
  @invalid_attrs %{name: nil, year: nil}

  def fixture(:car) do
    {:ok, car} = Cars.create_car(@create_attrs)
    car
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all cars", %{conn: conn} do
      conn = get(conn, Routes.car_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  defp create_car(_) do
    car = fixture(:car)
    %{car: car}
  end
end
