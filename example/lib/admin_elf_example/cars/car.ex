defmodule AdminElfExample.Cars.Car do
  use Ecto.Schema
  import Ecto.Changeset

  schema "cars" do
    field :name, :string
    field :year, :integer
    belongs_to :brand, AdminElfExample.Cars.Brand

    timestamps()
  end

  @doc false
  def changeset(car, attrs) do
    car
    |> cast(attrs, [:name, :year, :brand_id])
    |> validate_required([:name, :year, :brand_id])
  end
end

defmodule AdminElfExample.Cars.Car.QueryBuilder do
  use QueryElf,
    schema: AdminElfExample.Cars.Car,
    searchable_fields: ~w[id name year brand_id]a,
    sortable_fields: ~w[id name year]a,
    plugins: [QueryElf.Plugins.OffsetPagination]
end
