defmodule AdminElfExample.Cars.Brand do
  use Ecto.Schema
  import Ecto.Changeset

  schema "brands" do
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(brand, attrs) do
    brand
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end

defmodule AdminElfExample.Cars.Brand.QueryBuilder do
  use QueryElf,
    schema: AdminElfExample.Cars.Brand,
    searchable_fields: ~w[id name]a,
    sortable_fields: ~w[id name]a,
    plugins: [QueryElf.Plugins.OffsetPagination]
end
