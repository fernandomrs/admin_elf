defmodule AdminElfExampleWeb.BrandController do
  use AdminElfExampleWeb, :controller

  alias AdminElfExample.Cars
  alias AdminElfExample.Cars.Brand

  action_fallback AdminElfExampleWeb.FallbackController

  def index(conn, _params) do
    brands = Cars.list_brands()
    render(conn, "index.json", brands: brands)
  end

  def show(conn, %{"id" => id}) do
    brand = Cars.get_brand!(id)
    render(conn, "show.json", brand: brand)
  end
end
