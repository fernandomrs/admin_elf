defmodule AdminElfExampleWeb.Admin do
  @behaviour AdminElf.Admin

  alias AdminElfExampleWeb.Admin.{Brand, Car}

  def resources(_conn) do
    %{
      cars: %{
        car: Car,
        brand: Brand
      }
    }
  end

  def menu(_conn) do
    import AdminElf.MenuHelpers

    [
      resource(~w[cars car]a),
      resource(~w[cars brand]a)
    ]
  end
end
