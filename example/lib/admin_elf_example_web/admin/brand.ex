defmodule AdminElfExampleWeb.Admin.Brand do
  @behaviour AdminElf.Resource

  alias AdminElfExample.Cars

  def icon, do: "globe"
  def name, do: "brand"
  def plural_name, do: "brands"

  def id(brand), do: brand.id

  def filters(_conn) do
    import AdminElf.FilterHelpers

    [
      text_filter(:name__contains, label: "Search")
    ]
  end

  def list_resource(_conn, page, limit, order, filter) do
    {Cars.list_brands(filter, order: order, page: page, per_page: limit), Cars.count_brands(filter)}
  end

  def index_table(_conn) do
    import AdminElf.TableHelpers

    [
      field(:id),
      field(:name, order: true)
    ]
  end
end
