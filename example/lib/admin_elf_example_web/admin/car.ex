defmodule AdminElfExampleWeb.Admin.Car do
  @behaviour AdminElf.Resource

  alias AdminElfExample.Cars

  def icon, do: "truck"
  def name, do: "car"
  def plural_name, do: "cars"

  def id(car), do: car.id

  def filters(_conn) do
    import AdminElf.FilterHelpers

    [
      select_filter(:brand_id,
        label: "Brand",
        options: Enum.map(Cars.list_brands([], order: [asc: :name]), &{to_string(&1.id), &1.name})
      )
    ]
  end

  def list_resource(_conn, page, limit, order, filter) do
    {Cars.list_cars(filter, order: order, page: page, per_page: limit), Cars.count_cars(filter)}
  end

  def index_table(_conn) do
    import AdminElf.TableHelpers

    [
      field(:id),
      field(:name, order: true),
      field(:year),
      field(:brand, value: & &1.brand.name)
    ]
  end
end
