# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :admin_elf_example,
  ecto_repos: [AdminElfExample.Repo]

# Configures the endpoint
config :admin_elf_example, AdminElfExampleWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Bm91eLh5y5S66fNnoE6s4+C5fECU4M5yMB/OXHxa3weFkNt0cP1adX8euZ89VFRH",
  render_errors: [view: AdminElfExampleWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: AdminElfExample.PubSub,
  live_view: [signing_salt: "G6xzz+iM"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
