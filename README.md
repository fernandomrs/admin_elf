# AdminElf

A handy tool for all your administrative needs.

## Installation

TBD

## Contributing

There are several ways of contributing to the development of AdminElf, to name a few:

- If you find any issues while using this library, you can open an issue in the [issue tracker](https://gitlab.com/up-learn-uk/admin_elf/-/issues).
- You can check the [issue tracker](https://gitlab.com/up-learn-uk/admin_elf/-/issues) for unassigned issues to work on.

If you have an idea for a new feature, and wish to submit an MR for it, it is probably best to open an issue to discuss the idea first. This is an important step, as sometimes a feature idea would be better as an extra library instead of a part of AdminElf itself.

### Local development

To develop locally, you'll need to have NodeJS and Elixir installed locally. After that, you can fork this repo, clone it locally, and then run:

```sh
mix deps.get # install Elixir dependencies
cd assets && npm install && cd .. # install NodeJS dependencies 
mix test # run the tests
```

After that, you can make your changes, be sure that tests still pass, and open an MR to the main repository.

Sometimes we also need to actually execute the admin interface in a local environment, so that styling new features is easier. If you find yourself needing this, you can use the example app provided [here](./example).

To run it, make sure that you have access to a local PostgreSQL database. You can get one fairly easily with the following Docker command:

```sh
# for the initial setup
docker run -d --name admin_elf_example_database -e POSTGRES_PASSWORD=postgres -p 5432:5432 postgres

# for subsequent runs
docker start admin_elf_example_database
```

(if you are not using Docker, and are using a database that does not run on `localhost:5432` or that does not use the user/password `postgres/postgres`, be sure to change the configs at `example/config/dev.exs`)

After that you can setup and run the example application normally:

```sh
mix deps.get
mix ecto.setup
mix phx.server
```

Then on a separate terminal session, run the following to compile the front-end assets:

```sh
cd assets && npm run watch
```

Now you should be able to access the admin interface at http://localhost:4000/admin.