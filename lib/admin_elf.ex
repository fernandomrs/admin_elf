defmodule AdminElf do
  use Plug.Builder

  import Plug.Conn

  plug AdminElfWeb.Router

  def call(conn, opts) do
    conn
    |> put_private(__MODULE__, %{
      admin: Keyword.fetch!(opts, :admin)
    })
    |> super(opts)
  end
end
