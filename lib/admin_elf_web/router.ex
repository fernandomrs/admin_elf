defmodule AdminElfWeb.Router do
  use AdminElfWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AdminElfWeb do
    pipe_through :browser

    get "/", PageController, :index

    get "/:resource_id", ResourceController, :index
    # POST /:resource_id/:item_id/custom-action/:action
    # POST /:resource_id/custom-action/:action
    # GET /:resource_id/:item_id
    # GET /:resource_id/new
    # GET /:resource_id/:item_id/edit
    # POST /:resource_id
    # PATCH /:resource_id/:item_id
    # DELETE /:resource_id/:item_id
  end

  # Other scopes may use custom stacks.
  # scope "/api", AdminElfWeb do
  #   pipe_through :api
  # end
end
