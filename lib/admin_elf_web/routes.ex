defmodule AdminElfWeb.Routes do
  def static_path(_conn, path) do
    Path.join("/admin-elf", path)
  end

  def resource_index_path(conn, resource, params \\ []) do
    AdminElfWeb.Router.Helpers.resource_path(conn, :index, encode_resource(resource), params)
  end

  defp encode_resource(resource), do: Enum.join(resource, "__")
end
