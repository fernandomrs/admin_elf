defmodule AdminElfWeb.PageController do
  use AdminElfWeb, :controller

  def index(conn, _params) do
    IO.inspect(conn.private)

    render(conn, "index.html")
  end
end
