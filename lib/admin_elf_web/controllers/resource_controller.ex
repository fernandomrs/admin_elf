defmodule AdminElfWeb.ResourceController do
  use AdminElfWeb, :controller

  alias AdminElf.Resource

  def index(conn, %{"resource_id" => resource_id} = params) do
    admin = get_admin(conn)
    resource_id = resource_id |> String.split("__") |> Enum.map(&String.to_existing_atom/1)
    page = params |> Map.get("page", "1") |> String.to_integer()
    limit = params |> Map.get("limit", "25") |> String.to_integer()

    order =
      case params["order"] do
        %{"field" => field, "direction" => direction} when direction in ~w[asc desc] ->
          %{field: String.to_existing_atom(field), direction: String.to_existing_atom(direction)}

        %{"field" => field} ->
          %{field: String.to_existing_atom(field), direction: :asc}

        _ ->
          nil
      end

    filter = params |> Map.get("filter", %{}) |> parse_filters(conn, admin, resource_id)

    {items, count} = list_resource(conn, admin, resource_id, page, limit, order, filter)

    params = %{page: page, limit: limit, order: order, filter: filter}

    render(conn, "index.html", %{
      items: items,
      count: count,
      resource_id: resource_id,
      params: params
    })
  end

  defp get_admin(conn) do
    conn.private
    |> Map.fetch!(AdminElf)
    |> Map.fetch!(:admin)
  end

  defp list_resource(conn, admin, resource_id, page, limit, order, filter) do
    order =
      if order do
        [{order.direction, order.field}]
      else
        []
      end

    Resource.list_resource(conn, admin, resource_id, page, limit, order, filter)
  end

  defp parse_filters(raw_filters, conn, admin, resource_id) do
    conn
    |> Resource.get_filters(admin, resource_id)
    |> Enum.reduce(%{}, fn filter_definition, filters ->
      value = Map.get(raw_filters, to_string(filter_definition.name), "")

      cond do
        filter_definition.type in ~w[text select checkboxes]a and value != "" ->
          Map.put(filters, filter_definition.name, value)

        filter_definition.type == :radio ->
          value =
            if value == "" do
              filter_definition.default_option
            else
              value
            end

          Map.put(filters, filter_definition.name, value)

        filter_definition.type == :boolean and value in ~w[true false] ->
          Map.put(filters, filter_definition.name, value == "true")

        true ->
          filters
      end
    end)
  end
end
