defmodule AdminElfWeb.ViewHelpers do
  use Phoenix.HTML

  import Phoenix.View

  alias AdminElf.Resource
  alias AdminElfWeb.Routes
  alias AdminElfWeb.PartialView

  def capitalize(string) do
    String.capitalize(string)
  end

  def feather_icon(conn, icon, opts \\ []) do
    default_attrs = [
      width: "24",
      height: "24",
      fill: "none",
      stroke: "currentColor",
      stroke_width: "2",
      stroke_linecap: "round",
      stroke_linejoin: "round"
    ]

    {size, opts} = Keyword.pop(opts, :size)

    opts =
      if size do
        default_attrs
        |> Keyword.merge(opts)
        |> Keyword.merge(width: size, height: size)
      else
        Keyword.merge(default_attrs, opts)
      end

    content_tag(:svg, opts) do
      tag(:use, "xlink:href": Routes.static_path(conn, "/icons/feather-sprite.svg\##{icon}"))
    end
  end

  def resource_item_id(conn, resource_id, item) do
    conn
    |> Resource.get_item_id(get_admin(conn), resource_id, item)
    |> to_string()
  end

  def resource_name(conn, resource_id) do
    Resource.get_name(conn, get_admin(conn), resource_id)
  end

  def resource_plural_name(conn, resource_id) do
    Resource.get_plural_name(conn, get_admin(conn), resource_id)
  end

  def resource_icon(conn, resource_id, opts \\ []) do
    icon = Resource.get_icon(conn, get_admin(conn), resource_id)

    if icon do
      feather_icon(conn, icon, opts)
    end
  end

  def resource_filters(conn, resource_id) do
    Resource.get_filters(conn, get_admin(conn), resource_id)
  end

  def resource_index_table(conn, resource_id) do
    Resource.get_index_table(conn, get_admin(conn), resource_id)
  end

  def menu(conn) do
    get_admin(conn).menu(conn)
  end

  def resource_menu_active?(conn, resource_id) do
    Routes.resource_index_path(conn, resource_id) == conn.request_path
  end

  def update_resource_order(conn, resource_id, %{order: order} = params, order_field) do
    new_params =
      cond do
        not is_nil(order) and order.field == order_field and order.direction == :asc ->
          Map.put(params, :order, field: order_field, direction: :desc)

        not is_nil(order) and order.field == order_field and order.direction == :desc ->
          Map.put(params, :order, nil)

        true ->
          Map.put(params, :order, field: order_field, direction: :asc)
      end

    Routes.resource_index_path(conn, resource_id, new_params)
  end

  def render_partial(partial, args) do
    render(PartialView, partial, args)
  end

  defp get_admin(conn) do
    conn.private
    |> Map.fetch!(AdminElf)
    |> Map.fetch!(:admin)
  end
end
