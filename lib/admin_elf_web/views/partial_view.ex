defmodule AdminElfWeb.PartialView do
  use AdminElfWeb, :view

  defp generate_pages(page, limit, last_page) do
    [
      [1],
      [page - 2, page - 1, page, page + 1, page + 2],
      [last_page]
    ]
    |> List.flatten()
    |> Enum.filter(&(&1 > 0))
    |> Enum.filter(&(&1 <= last_page))
    |> Enum.uniq()
    |> Enum.chunk_every(2, 1)
    |> Enum.flat_map(fn
      [page1, page2] when page1 + 1 == page2 -> [page1]
      [page1, _page2] -> [page1, "..."]
      [page] -> [page]
    end)
  end
end
