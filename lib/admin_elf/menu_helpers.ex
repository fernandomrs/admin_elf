defmodule AdminElf.MenuHelpers do
  def dashboard do
    %{type: :dashboard}
  end

  def resource(id) do
    %{type: :resource, resource_id: id}
  end

  def group(title, opts) do
    %{
      type: :group,
      title: title,
      resources: Keyword.fetch!(opts, :resources),
      icon: Keyword.get(opts, :icon)
    }
  end
end
