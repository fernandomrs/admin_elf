defmodule AdminElf.Admin do
  @type resources_map() :: %{required(atom()) => module() | resources_map()}

  @type menu_item() ::
          %{type: :dashboard}
          | %{type: :resource, resource: String.t()}
          | %{
              type: :group,
              title: String.t(),
              icon: String.t() | nil,
              resources: [[Atom.t()]]
            }

  @type menu_definition() :: [menu_item()]

  @callback resources(conn :: Plug.Conn.t()) :: resources_map()
  @callback menu(conn :: Plug.Conn.t()) :: menu_definition()
end
