defmodule AdminElf.TableHelpers do
  @type field() :: %{
          field: atom(),
          label: String.t(),
          value: (term() -> term()),
          order: atom()
        }

  @spec field(atom()) :: field()
  def field(field, opts \\ []) do
    order =
      case Keyword.get(opts, :order, false) do
        true -> field
        value -> value
      end

    label = Keyword.get(opts, :label, field |> to_string() |> String.capitalize())

    %{
      field: field,
      label: label,
      value: Keyword.get(opts, :value, &Map.fetch!(&1, field)),
      order: order
    }
  end
end
