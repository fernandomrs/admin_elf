defmodule AdminElf.Resource do
  @callback id(term()) :: String.t()
  @callback name() :: String.t()
  @callback plural_name() :: String.t()
  @callback icon() :: String.t() | nil
  @callback list_resource(
              conn :: Plug.Conn.t(),
              page :: pos_integer(),
              limit :: pos_integer(),
              order :: [{atom(), atom()}],
              filter :: map()
            ) :: {items :: [term()], count :: pos_integer()}

  @callback filters(conn :: Plug.Conn.t()) :: [AdminElf.FiltersHelpers.filter()]
  @callback index_table(conn :: Plug.Conn.t()) :: [AdminElf.TableHelpers.field()]

  def get_item_id(conn, admin, id, item) do
    fetch_resource!(conn, admin, id).id(item)
  end

  def get_name(conn, admin, id) do
    fetch_resource!(conn, admin, id).name()
  end

  def get_plural_name(conn, admin, id) do
    fetch_resource!(conn, admin, id).plural_name()
  end

  def get_icon(conn, admin, id) do
    fetch_resource!(conn, admin, id).icon()
  end

  def get_filters(conn, admin, id) do
    fetch_resource!(conn, admin, id).filters(conn)
  end

  def get_index_table(conn, admin, id) do
    fetch_resource!(conn, admin, id).index_table(conn)
  end

  def list_resource(conn, admin, id, page, limit, order, filter) do
    fetch_resource!(conn, admin, id).list_resource(conn, page, limit, order, filter)
  end

  defp fetch_resource!(conn, admin, id) do
    path = Enum.map(id, &Access.key!/1)

    get_in(admin.resources(conn), path)
  end
end
