defmodule AdminElf.Plug do
  use Plug.Builder

  import Plug.Conn

  plug AdminElfWeb.Router

  def call(conn, opts) do
    conn
    |> put_private(AdminElf, %{
      admin: Keyword.fetch!(opts, :admin)
    })
    |> super(opts)
  end
end
