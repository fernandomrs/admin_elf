defmodule AdminElf.FilterHelpers do
  @type text_filter() :: %{type: :text, name: atom(), label: String.t()}
  @type number_filter() :: %{type: :number, name: atom(), label: String.t()}
  @type select_filter() :: %{
          type: :select,
          name: atom(),
          label: String.t(),
          options: [{value :: String.t(), label :: String.t()}]
        }
  @type radio_filter() :: %{
          type: :radio,
          name: atom(),
          label: String.t(),
          default_option: String.t(),
          options: [{value :: String.t(), label :: String.t()}]
        }
  @type boolean_filter() :: %{
          type: :boolean,
          name: atom(),
          label: String.t()
        }
  @type checkboxes_filter() :: %{
          type: :checkboxes,
          name: atom(),
          label: String.t(),
          options: [{value :: String.t(), label :: String.t()}]
        }

  @type filter() :: text_filter() | number_filter() | select_filter() | radio_filter()

  def text_filter(name, opts \\ []) do
    filter(name, :text,
      label: Keyword.get(opts, :label, name |> to_string() |> String.capitalize())
    )
  end

  def number_filter(name, opts \\ []) do
    filter(name, :number,
      label: Keyword.get(opts, :label, name |> to_string() |> String.capitalize())
    )
  end

  def select_filter(name, opts \\ []) do
    filter(name, :select,
      label: Keyword.get(opts, :label, name |> to_string() |> String.capitalize()),
      options: Keyword.fetch!(opts, :options)
    )
  end

  def radio_filter(name, opts \\ []) do
    options = Keyword.fetch!(opts, :options)

    filter(name, :radio,
      label: Keyword.get(opts, :label, name |> to_string() |> String.capitalize()),
      options: options,
      default_option: Keyword.get(opts, :default_option, options |> List.first() |> elem(0))
    )
  end

  def boolean_filter(name, opts \\ []) do
    filter(name, :boolean,
      label: Keyword.get(opts, :label, name |> to_string() |> String.capitalize())
    )
  end

  def checkboxes_filter(name, opts \\ []) do
    filter(name, :checkboxes,
      label: Keyword.get(opts, :label, name |> to_string() |> String.capitalize()),
      options: Keyword.fetch!(opts, :options)
    )
  end

  defp filter(name, type, opts) do
    opts
    |> Map.new()
    |> Map.merge(%{name: name, type: type})
  end
end
